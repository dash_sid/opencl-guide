/* Listing 2.6 - Searching for a kernel by name */

#define KERNEL_FILE "test.cl"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define CL_TARGET_OPENCL_VERSION 120
#ifdef MAC
#include <OpenCL/cl.h>
#else
#include <CL/cl.h>
#endif

int main(int argc, char const *argv[]) {

  /* Host/Device data structures */
  cl_platform_id platform;
  cl_device_id device;
  cl_context context;
  cl_program program;
  cl_int err;
  cl_uint i;

  /* Program/Kernel data structures */
  cl_kernel *kernels, found_kernel;
  cl_uint num_kernels;
  char kernel_name[20];
  FILE *program_handle;
  char *program_buffer, *program_log;
  size_t program_size, log_size;

  /* Access the first installed platform */
  err = clGetPlatformIDs(1, &platform, NULL);
  if (err < 0) {
    perror("Couldn't find any platforms.");
    exit(1);
  }

  /* Access the CPU / GPU device */
  err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 1, &device, NULL);
  if (err == CL_DEVICE_NOT_FOUND) {
    err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_CPU, 1, &device, NULL);
  }
  if (err < 0) {
    perror("Couldn't find any device.");
    exit(1);
  }

  /* Create a context */
  context = clCreateContext(NULL, 1, &device, NULL, NULL, &err);
  if (err < 0) {
    perror("Couldn't create a context.");
    exit(1);
  }

  /* Read the kernel file and store the contents into a buffer */
  program_handle = fopen(KERNEL_FILE, "r");
  if (program_handle == NULL) {
    perror("Couldn't open the kernel file.");
    exit(1);
  }
  fseek(program_handle, 0, SEEK_END);
  program_size = ftell(program_handle);
  rewind(program_handle);

  /* Allocate the program's buffer and store the contents of the file into the buffer */
  program_buffer = (char*) malloc(program_size+1);
  program_buffer[program_size] = '\0';
  fread(program_buffer, sizeof(char), program_size, program_handle);
  fclose(program_handle);

  /* Create a program from the kernel file */
  program = clCreateProgramWithSource(context, 1, (const char**)&program_buffer, &program_size, &err);
  if (err < 0) {
    perror("Couldn't create the program.");
    exit(1);
  }

  /* Build the program */
  err = clBuildProgram(program, 1, &device, NULL, NULL, NULL);
  if (err < 0) {
    printf("Build failure!!!\n");
    /* Get build log information */
    clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);
    program_log = (char*) malloc(log_size+1);
    program_log[log_size] = '\0';
    clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, log_size+1, program_log, NULL);
    printf("%s\n", program_log);
    free(program_log);
    exit(1);
  }

  /* Find out the number of kernels in the source (kernel) file */
  err = clCreateKernelsInProgram(program, 0, NULL, &num_kernels);
  if (err < 0) {
    perror("Couldn't find any kernels.");
    exit(1);
  }
  
  /* Create a kernel for each function */
  kernels = (cl_kernel*) malloc(num_kernels * sizeof(cl_kernel));
  clCreateKernelsInProgram(program, num_kernels, kernels, NULL);

  /* Search for the named kernel */
  for (i = 0; i < num_kernels; i++) {
    clGetKernelInfo(kernels[i], CL_KERNEL_FUNCTION_NAME, sizeof(kernel_name), kernel_name, NULL);
    if (strcmp(kernel_name, "mul") == 0) {
      found_kernel = kernels[i];
      printf("Found the %s kernel at index %u.\n", kernel_name, i);
      break;
    }
  }

  /* Deallocate resource */
  for (i = 0; i < num_kernels; i++) {
    clReleaseKernel(kernels[i]);
  }
  free(kernels);
  clReleaseProgram(program);
  clReleaseContext(context);
  
  return 0;
}
