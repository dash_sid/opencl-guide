/* Listing 2.2 - Testing Device Extensions */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define CL_TARGET_OPENCL_VERSION 120
#ifdef MAC
#include <OpenCL/cl.h>
#else
#include <CL/cl.h>
#endif

int main(int argc, char const *argv[]) {

  cl_platform_id platform;
  cl_device_id *devices;
  cl_uint num_devices, addr_data;
  cl_int i, err;
  char name_data[48], ext_data[4096];

  /* Access the first platform */
  err = clGetPlatformIDs(1, &platform, NULL);
  if (err < 0) {
    perror("Couldn't find any platforms that support OpenCL.");
    exit(1);
  }

  /* Find the number of devices */
  err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL, 1, NULL, &num_devices);
  if (err < 0) {
    perror("Couldn't find any devices.");
    exit(1);
  }

  /* Allocate device memory */
  devices = (cl_device_id*) malloc(sizeof(cl_device_id) * num_devices);

  /* Populate device memory */
  clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL, num_devices, devices, NULL);

  /* Get device information */
  for (i = 0; i < num_devices; i++) {
    // Find the device name
    err = clGetDeviceInfo(devices[i], CL_DEVICE_NAME, sizeof(name_data), name_data, NULL);
    if (err < 0) {
      perror("Couldn't read extension data.");
      exit(1);
    }

    /* Find the address width of the device */
    clGetDeviceInfo(devices[i], CL_DEVICE_ADDRESS_BITS, sizeof(ext_data), &addr_data, NULL);

    /* Find extensions supported by the device */
    clGetDeviceInfo(devices[i], CL_DEVICE_EXTENSIONS, sizeof(ext_data), ext_data, NULL);

    printf("NAME: %s\n ADDRESS_WIDTH: %u\n EXTENSIONS: %s\n", name_data, addr_data, ext_data);
  }

  /* Deallocate resources */
  free(devices);

  return 0;
}
